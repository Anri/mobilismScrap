package com.mylloon.mobidl

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

var sharedPrefName = "com.mylloon.MobiDL" // shared pref name

class BackgroundUpdateCheck(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {
    private val ctx = appContext

    override fun doWork(): Result {
        fun getValuesAppNeedToBeUpdated(): MutableMap<String, Boolean> { // list of the apps (from the storage if exists)
            val context = MainActivity.applicationContext() // get app context

            // read apps from the app preference
            val sharedPref = context.getSharedPreferences(sharedPrefName,
                AppCompatActivity.MODE_PRIVATE)
            val dataApp: Set<String>? = sharedPref.getStringSet("appsUpdate", null)
            val dataBool: Set<String>? = sharedPref.getStringSet("boolUpdate", null)
            val dataAppList = dataApp?.toList()
            val dataBoolList = dataBool?.toList()

            val finalMap = mutableMapOf<String, Boolean>()
            if ((dataAppList != null) and (dataBoolList != null)) {
                for (i in dataAppList!!.indices) {
                    if (i in dataBoolList!!.indices) {
                        finalMap[dataAppList[i]] =
                            dataBoolList[i].substring(i.toString().length).toBoolean()
                    }
                }
            }
            return finalMap
        }
        val apps = getValuesAppNeedToBeUpdated()
        for (app in apps.keys) {
            if (apps[app] == true) {
                println("Checking update for $app...")
                Notif().work(ctx, app)
            }
        }
        println("Update check: Done.")

        return Result.success()
    }
}

class Notif {
    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationChannel: NotificationChannel
    private lateinit var builder: Notification.Builder
    private val channelId = "i.apps.notifications"
    private val description = R.string.descriptionNotification

    @SuppressLint("UnspecifiedImmutableFlag")
    fun newNotification(
        context: Context,
        notificationID: Int,
        appName: String,
        version: String,
        url: String,
    ) {
        notificationManager = context.getSystemService(NotificationManager::class.java)
        val pendingIntent = PendingIntent.getActivity(context,
            notificationID,
            Intent(Intent.ACTION_VIEW, Uri.parse(url)),
            (PendingIntent.FLAG_UPDATE_CURRENT))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { // checking if android version is greater than oreo(API 26) or not
            notificationChannel = NotificationChannel(channelId,
                context.getString(description),
                NotificationManager.IMPORTANCE_LOW)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(context, channelId)
                .setContentTitle("${context.getString(R.string.newUpdateTitleNotification)} $appName")
                .setContentText("${context.getString(R.string.newUpdateVersionNotification)} $version ${
                    context.getString(R.string.newUpdateAvailableNotification)
                }")
                .setSmallIcon(R.drawable.download)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
        } else {
            builder = Notification.Builder(context)
                .setContentTitle("${context.getString(R.string.newUpdateTitleNotification)} $appName")
                .setContentText("${context.getString(R.string.newUpdateVersionNotification)} $version ${
                    context.getString(R.string.newUpdateAvailableNotification)
                }")
                .setSmallIcon(R.drawable.download)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
        }
        notificationManager.notify(notificationID, builder.build())
        val sharedPref =
            context.getSharedPreferences(sharedPrefName, AppCompatActivity.MODE_PRIVATE)
        sharedPref.edit().putInt("notifID", notificationID + 1).apply()
    }

    fun work(context: Context, app: String) {
        var appName = app
        val registeredAnswered: Map<String, String> = Gson().fromJson(
            context.assets.open("appNames.json").bufferedReader()
                .use { it.readText() },
            object : TypeToken<Map<String, String>>() {}.type
        )
        for ((key, value) in registeredAnswered) appName =
            if (Regex(appName.lowercase()).containsMatchIn(key)) value else appName.lowercase()
        var installedAppVersion: String? = null
        for (packageInfo in context.packageManager.getInstalledPackages(PackageManager.GET_META_DATA)) {
            if (Regex(appName).containsMatchIn(packageInfo.packageName)) installedAppVersion =
                packageInfo.versionName
        }
        if (installedAppVersion == null) return
        else {
            try {
                val res =
                    Scraper(Credentials().get(0), Credentials().get(1), app).search() ?: return
                if (res[0].containsKey("gotResults")) {
                    var latestVersion = res[1]["title"]!!
                    latestVersion = Regex("""(?<=v?)\d+\.\d+(\.\d+)?""").findAll(latestVersion)
                        .map { it.groupValues[0] }.toList()[0]
                    val arrayInstalledVersion: MutableList<String> =
                        installedAppVersion.split(".") as MutableList<String>
                    val arrayLatestVersion: MutableList<String> =
                        latestVersion.split(".") as MutableList<String>
                    while (arrayInstalledVersion.size > arrayLatestVersion.size) arrayLatestVersion.add(
                        "0")
                    while (arrayInstalledVersion.size < arrayLatestVersion.size) arrayInstalledVersion.add(
                        "0")
                    var needUpdate = false
                    for (i in arrayInstalledVersion.indices) {
                        if (arrayLatestVersion[i].toInt() > arrayInstalledVersion[i].toInt()) needUpdate =
                            true
                    }
                    if (needUpdate) {
                        val sharedPref = context.getSharedPreferences(sharedPrefName,
                            AppCompatActivity.MODE_PRIVATE)
                        return newNotification(context,
                            sharedPref.getInt("notifID", 0),
                            app,
                            latestVersion,
                            "https://forum.mobilism.org${res[1]["link"]}")
                    }
                }
            } catch (e: Exception) {
                return
            }
        }
    }
}