package com.mylloon.mobidl

import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result.Failure as FuelFailure
import com.github.kittinunf.result.Result.Success as FuelSuccess

class Scraper(
    private var pseudo: String? = null, // can be null if parsing a post
    private var password: String? = null, // can be null if parsing a post
    private var app: String? = null, // can be null if parsing a post
    private var captchaID: String? = null, // can be null if no captcha
    private var captchaResponse: String? = null, // can be null if no captcha
) {

    private var url: String = "https://forum.mobilism.org"
    private var sid: String? = Credentials().getSID()
    private var retry: Int = 0

    private var errorNotConnected = "Log me on automatically each visit"
    private var loginAttemptsExceeded = "You exceeded the maximum allowed number of login attempts."
    private var searchNotLogged =
        "Sorry but you are not permitted to use the search system. If you're not logged in please"

    private fun connect(): MutableList<Map<String, String?>>? { // Login to the forum using credentials.
        println("Connection attempt...")
        retry++
        FuelManager.instance.basePath = url
        FuelManager.instance.baseParams = listOf("mode" to "login")
        val params = mutableListOf("username" to pseudo, "password" to password, "login" to "Login")
        if (captchaID != null) {
            params.add("qa_answer" to captchaResponse.toString())
            params.add("qa_confirm_id" to captchaID.toString())
        }
        val session = "/ucp.php"
            .httpPost(params)
            .responseString { _, response, result ->
                when (result) {
                    is FuelFailure -> {
                        val ex = result.getException()
                        println("[${response.statusCode}] Exception: $ex")
                    }
                    is FuelSuccess -> {
                        println("") // does nothing but is required otherwise everything bugs :(
                    }
                }
            }
        val data = session.join().data.decodeToString()
        sid = "(?<=sid=)(.{32})".toRegex().find(data)?.value
        if (errorNotConnected in data) return error(data)
        Credentials().storeSID(sid.toString())
        return search()
    }

    private fun error(htmlPage: String): MutableList<Map<String, String?>>? { // Handle connection errors
        var array: MutableList<Map<String, String?>>? = null
        if (errorNotConnected in htmlPage) {
            if (loginAttemptsExceeded in htmlPage) {
                val qaConfirmID =
                    "(?<=qa_confirm_id\" value=\")(.{32})".toRegex().find(htmlPage)?.value
                var question = "(?<=answer\">)(.*)</l".toRegex().find(htmlPage)?.value
                if (question != null) {
                    question = question.dropLast(3)
                }
                array = mutableListOf(
                    mutableMapOf(
                        "loginAttemptsExceeded" to null,
                        "question" to question,
                        "qaConfirmID" to qaConfirmID
                    )
                )
            } else {
                Credentials().delete()
                array = mutableListOf(mutableMapOf("errorNotConnected" to null))
            }
        }
        return array
    }

    fun search(): MutableList<Map<String, String?>>? { // Do the research.
        if (sid == null) {
            println("SID not found")
            return if (retry < 3) connect() else mutableListOf(mutableMapOf("noSID" to null))
        } else {
            println("SID recovered")
            retry = 0
        }
        println("Going to search page...")
        FuelManager.instance.basePath = url
        FuelManager.instance.baseParams =
            listOf("sid" to sid, "sr" to "topics", "sf" to "titleonly")
        val session = "/search.php"
            .httpGet(listOf("keywords" to app))
            .responseString { _, response, result ->
                when (result) {
                    is FuelFailure -> {
                        val ex = result.getException()
                        println("[${response.statusCode}] Exception: $ex")
                    }
                    is FuelSuccess -> {
                        println("") // does nothing but is required otherwise everything bugs :(
                    }
                }
            }
        val data = session.join().data.decodeToString()
        return if (searchNotLogged in data) {
            println("The SID doesn't work, new attempt...")
            if (retry < 3) connect() else mutableListOf(mutableMapOf("badSID" to null))
        } else parse(data)
    }

    private fun parse(htmlPage: String): MutableList<Map<String, String?>> { // Parse HTML response to a clean list.
        println("Fetching results for $app...")
        // println(htmlPage)
        if ("No suitable matches were found." in htmlPage) return mutableListOf(mutableMapOf("noResults" to null))
        val elements: MutableList<String> = htmlPage.split("<tr>\n<td>").toMutableList()
        val finalElements = mutableListOf<Map<String, String?>>()
        elements.removeFirst()
        val lastIndex = elements.toList().lastIndex
        elements[lastIndex] = elements[lastIndex].split("</td>\n</tr>")[0]
        finalElements.add(0, mapOf("gotResults" to null))
        for (i in elements.indices) {
            val title: String? = try {
                Regex("""class="topictitle">(.*)</a>""").findAll(elements[i])
                    .map { it.groupValues[1] }.toList()[0].replace(Regex("( ?&amp; ?)"), " & ")
            } catch (e: Exception) {
                null
            }
            val author: String? = try {
                val regex =
                    """(<br />|</strong>)\n\n?<i class="icon-user"></i> by <a href="\./memberlist\.php\?mode=viewprofile&amp;u=\d+&amp;sid=.+"( style="color: #.*;" class="username-coloured")?>(.*)</a>"""
                Regex(regex).findAll(elements[i]).map { it.groupValues.last() }.toList()[0]
            } catch (e: Exception) {
                null
            }
            val link: String? = try {
                val temp =
                    Regex("""\./viewtopic\.php\?f=(\d*)&amp;t=(\d*)&amp""").findAll(elements[i])
                "/viewtopic.php?f=${
                    temp.map { it.groupValues[1] }.toList()[0]
                }&t=${temp.map { it.groupValues[2] }.toList()[0]}"
            } catch (e: Exception) {
                null
            }
            val date: String? = try {
                Regex("""</a> <i class="icon-time"></i> <small>(.*)</small>""").findAll(elements[i])
                    .map { it.groupValues[1] }.toList()[0]
            } catch (e: Exception) {
                null
            }
            finalElements.add(
                i + 1,
                mapOf("title" to title, "author" to author, "link" to link, "date" to date)
            )
        }
        println("Search parsed!")
        return finalElements
    }

    fun parseInfos(urlPost: String): MutableMap<String, String?> {
        println("Going to post page...")
        FuelManager.instance.basePath = url
        val session = urlPost
            .httpGet()
            .responseString { _, response, result ->
                when (result) {
                    is FuelFailure -> {
                        val ex = result.getException()
                        println("[${response.statusCode}] Exception: $ex")
                    }
                    is FuelSuccess -> {
                        println("Post page retrieved") // does nothing but is required otherwise everything bugs :(
                    }
                }
            }
        var htmlPage = session.join().data.decodeToString()
        val elements = mutableMapOf<String, String?>()
        // if ("Download Instructions" !in htmlPage) {
        //     elements["changelogs"] = null
        //     elements["downloadLinks"] = null
        // }
        elements["changelogs"] = try {
            var tmp =
                Regex("""What's New:</span> ?<br />(.*)<br /><br /><span style="c|font-weight: bold">T""").findAll(
                    htmlPage)
                    .map { it.groupValues[1] }.toList()[0]
            if (tmp.length < 2) { // if result none, trying other method
                tmp =
                    Regex("""What's New:</span> ?<br />(.*)<br /><br /><span style="font-weight: bold">T""").findAll(
                        htmlPage)
                        .map { it.groupValues[1] }.toList()[0]
            }
            tmp.replace(Regex("""<br />\n?"""), "\n") // convert newline html to \n
        } catch (e: Exception) {
            "No changelog found."
        }
        elements["downloadLinks"] = try {
            htmlPage = htmlPage.replace(Regex("Download Instructions:</span>(.*?)?<br />"),
                "Download Instructions:</span><br />")
            var tmp =
                Regex("""Download Instructions:</span> ?<br />(.*|[\s\S]*)<br /><br />Trouble downloading|</a></div>""").findAll(
                    htmlPage)
                    .map { it.groupValues[1] }.toList()[0]
            if (tmp.length < 2) { // if result none, trying other method
                tmp =
                    Regex("""Download Instructions:</span> ?<br />(.*|[\s\S]*)</a></div>""").findAll(
                        htmlPage)
                        .map { it.groupValues[1] }.toList()[0]
            }
            tmp =
                tmp.replace(Regex("""\n|<a class="postlink" href="|\(Closed Filehost\) ?|<span style="font-weight: bold">|</span>|">(\S*)</a>"""),
                    "") // remove html garbage
            tmp = tmp.replace(Regex("""<br />\n?"""), "\n") // convert newline html to \n
            tmp = tmp.replace(Regex("""Mirrors(?!:)|Mirror(?!s)(?!:)"""), "Mirror:") // add ":"
            var finalTmp = ""
            for (i in tmp.split("\n")) finalTmp += i.split(">")[0] + "\n"
            finalTmp.replace("\"", "")
        } catch (e: Exception) {
            null
        }
        return elements
    }

    fun parseDownloadLinks(links: String): MutableMap<String, List<String>> {
        val downloadLinks = links.split("\n").filter { it != "" }
        val categoryDL = mutableMapOf<String, List<String>>()
        var inCategory: String? = null
        val noCategoryString = "No Category"
        val tempNoCategoryDL = mutableListOf<String>()
        val tempCategoryDL = mutableListOf<String>()
        categoryDL[noCategoryString] = listOf("") // init the no category at index 0 of the map
        for (downloadLink in downloadLinks) {
            if (("http" in downloadLink) or ("www." in downloadLink)) { // lien
                if (inCategory != null) tempCategoryDL.add(downloadLink) // if the link is inside a category
                else tempNoCategoryDL.add(downloadLink) // else he don't have category
            } else { // else it's a category
                if ((inCategory != null) && (tempCategoryDL.size > 0)) { // already in category?
                    categoryDL[inCategory] = tempCategoryDL.toMutableList()
                    tempCategoryDL.clear()
                }
                var previous = ""
                if (("Mirror" in downloadLink) and (inCategory != null)) previous =
                    " of $inCategory"
                inCategory = "${downloadLink.replace(":", "")}$previous" // first category
            }
        }
        if (tempCategoryDL.isNotEmpty()) categoryDL[inCategory!!] = tempCategoryDL
        if (tempNoCategoryDL.isNotEmpty()) categoryDL[noCategoryString] =
            tempNoCategoryDL else categoryDL.remove(noCategoryString)

        return categoryDL
    }

}
