package com.mylloon.mobidl

import android.content.Context.MODE_PRIVATE
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.PrivateKey
import java.security.PublicKey
import javax.crypto.Cipher


class Credentials {

    private val provider = "AndroidKeyStore" // location of the key
    private val aliasKeyStore = "MobiDL" // name of the key
    private val transformation = "RSA/ECB/PKCS1Padding" // 'type' of the key
    private val sharedPref = "com.mylloon.MobiDL" // shared pref name

    fun generateKey() { // generate RSA Keys
        val keyPairGenerator = KeyPairGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_RSA, provider
        )
        keyPairGenerator.initialize(
            KeyGenParameterSpec.Builder(
                aliasKeyStore,
                KeyProperties.PURPOSE_DECRYPT
            )
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                .build()
        )
        keyPairGenerator.generateKeyPair()
    }

    private fun getPublicKey(): PublicKey { // Get the Public RSA key from the Android KeyStore
        val keyStore: KeyStore = KeyStore.getInstance(provider)
        keyStore.load(null)
        return keyStore.getCertificate(aliasKeyStore).publicKey
    }

    private fun getPrivateKey(): PrivateKey { // Get the Private RSA key from the Android KeyStore
        val keyStore: KeyStore = KeyStore.getInstance(provider)
        keyStore.load(null)
        return keyStore.getKey(aliasKeyStore, null) as PrivateKey
    }

    private fun encrypt(message: String): ByteArray { // Encrypt a string with the public RSA key
        val cipher = Cipher.getInstance(transformation)
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey())

        return cipher.doFinal(message.toByteArray())
    }

    private fun decrypt(message: ByteArray): String { // Decrypt an encrypted string with the private RSA key
        val cipher = Cipher.getInstance(transformation)
        cipher.init(Cipher.DECRYPT_MODE, getPrivateKey())

        val decryptedBytes = cipher.doFinal(message)
        return decryptedBytes.decodeToString()
    }

    fun store(
        user: String,
        password: String
    ) { // Store combo encrypted user/pass in shared preferences
        val userEncrypted = encrypt(user) // encrypt user
        val passwordEncrypted = encrypt(password) // encrypt password
        val context = MainActivity.applicationContext() // get app context

        // show key : Base64.getEncoder().encodeToString(encryptedData)

        // write encrypted data to the app preference
        val sharedPref = context.getSharedPreferences(sharedPref, MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putString("username", userEncrypted.contentToString())
            this?.putString("password", passwordEncrypted.contentToString())
            this?.apply()
        }
    }

    fun get(type: Int): String? { // type 0 -> username | type 1 -> password [ return null if there is nothing
        val context = MainActivity.applicationContext() // get app context

        // read encrypted data from the app preference
        val sharedPref = context.getSharedPreferences(sharedPref, MODE_PRIVATE)
        var data: String? = null
        when (type) {
            0 -> {
                data = sharedPref.getString("username", null)
            }
            1 -> {
                data = sharedPref.getString("password", null)
            }
        }

        return if (data != null) {
            val split: List<String> =
                data.substring(1, data.length - 1).split(", ")
            val array = ByteArray(split.size)
            for (i in split.indices) {
                array[i] = split[i].toByte()
            }
            decrypt(array)
        } else null
    }

    fun storeSID(sid: String) {
        val sidEncrypted = encrypt(sid) // encrypt sid
        val context = MainActivity.applicationContext() // get app context

        // write encrypted data to the app preference
        val sharedPref = context.getSharedPreferences(sharedPref, MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putString("sid", sidEncrypted.contentToString())
            this?.apply()
        }
    }

    fun getSID(): String? {
        val context = MainActivity.applicationContext() // get app context

        // read encrypted data from the app preference
        val sharedPref = context.getSharedPreferences(sharedPref, MODE_PRIVATE)
        val data: String? = sharedPref.getString("sid", null)

        return if (data != null) {
            val split: List<String> =
                data.substring(1, data.length - 1).split(", ")
            val array = ByteArray(split.size)
            for (i in split.indices) {
                array[i] = split[i].toByte()
            }
            decrypt(array)
        } else null
    }

    fun delete() {
        val context = MainActivity.applicationContext() // get app context

        // clear shared pref
        val sharedPref = context.getSharedPreferences(sharedPref, MODE_PRIVATE)
        with(sharedPref.edit()) {
            this.remove("username")
            this.remove("password")
            this.remove("sid")
            this.apply()
        }
    }
}
