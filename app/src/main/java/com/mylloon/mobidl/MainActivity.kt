package com.mylloon.mobidl


import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.ScrollingMovementMethod
import android.util.DisplayMetrics
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit

private fun isWorkScheduled(context: Context): Boolean {
    val instance = WorkManager.getInstance(context)
    val statuses = instance.getWorkInfosByTag("com.mylloon.mobidl.BackgroundUpdateCheck")
    return try {
        var running = false
        val workInfoList = statuses.get()
        for (workInfo in workInfoList) {
            val state = workInfo.state
            running = (state == WorkInfo.State.RUNNING) or (state == WorkInfo.State.ENQUEUED)
        }
        running
    } catch (e: ExecutionException) {
        e.printStackTrace()
        println("No job already running")
        false
    } catch (e: InterruptedException) {
        e.printStackTrace()
        println("No job already running")
        false
    }
}

fun newJob(context: Context) {
    val myConstraints = Constraints.Builder()
        .setRequiredNetworkType(NetworkType.CONNECTED) //checks whether device should have Network Connection
        .build()
    val job = PeriodicWorkRequestBuilder<BackgroundUpdateCheck>(1, TimeUnit.DAYS)
        .setConstraints(myConstraints)
        .build()
    WorkManager.getInstance(context).enqueue(job)
}

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (!isWorkScheduled(context)) {
            println("Create a update check job in background...")
            when (intent.action) {
                Intent.ACTION_DATE_CHANGED -> {
                    newJob(context)
                }
                Intent.ACTION_BOOT_COMPLETED -> {
                    val sharedPref = context.getSharedPreferences("com.mylloon.MobiDL",
                        AppCompatActivity.MODE_PRIVATE)
                    sharedPref.edit().putInt("notifID", 0).apply()
                    newJob(context)
                }
            }
        }
    }
}

class MainActivity : AppCompatActivity() {
    private var settingsButton: Menu? = null // before starting the app there is no settings button
    private var settingsButtonVisible: Boolean = false
    private var inSettings: Boolean = false // by default your not in settings page
    private var inAppList: Boolean = false // by default your not in app list page
    private var inAppInfo: Boolean = false // by default your not in app info page
    private var sharedPref: SharedPreferences? = null // first run detection
    private val sharedPrefName = "com.mylloon.MobiDL" // shared pref name
    private var timeOfLastToast: Long = System.currentTimeMillis() - 2000
    private var listInfos: MutableList<Map<String, String?>>? = null
    private var appMobilismInfos: Map<String, String?>? = null

    companion object {
        private var instance: MainActivity? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    init {
        instance = this
    }

    private fun toggleSettingsButtonVisibility() { // Change Settings button visibility
        settingsButtonVisible = settingsButton?.findItem(R.id.settingsButton)?.isVisible == false
        settingsButton?.findItem(R.id.settingsButton)?.isVisible = settingsButtonVisible
    }

    private fun getValuesRecyclerView(): MutableList<String> { // list of the apps (from the storage if exists)
        val context = applicationContext() // get app context

        // read apps from the app preference
        val sharedPref = context.getSharedPreferences(sharedPrefName, MODE_PRIVATE)
        val data: Set<String>? = sharedPref.getStringSet("apps", null)

        return data?.toMutableList() ?: mutableListOf()
    }


    private fun getValuesAppNeedToBeUpdated(): MutableMap<String, Boolean> { // list of the apps (from the storage if exists)
        val context = applicationContext() // get app context

        // read apps from the app preference
        val sharedPref = context.getSharedPreferences(sharedPrefName, MODE_PRIVATE)
        val dataApp: Set<String>? = sharedPref.getStringSet("appsUpdate", null)
        val dataBool: Set<String>? = sharedPref.getStringSet("boolUpdate", null)
        val dataAppList = dataApp?.toList()
        val dataBoolList = dataBool?.toList()

        val finalMap = mutableMapOf<String, Boolean>()
        if ((dataAppList != null) and (dataBoolList != null)) {
            for (i in dataAppList!!.indices) {
                if (i in dataBoolList!!.indices) {
                    finalMap[dataAppList[i]] =
                        dataBoolList[i].substring(i.toString().length).toBoolean()
                }
            }
        }
        return finalMap
    }

    private fun storeValuesAppNeedToBeUpdated(updateApps: MutableMap<String, Boolean>) {
        val updateAppsName: Set<String> = updateApps.keys
        val updateAppsBool: MutableCollection<Boolean> = updateApps.values
        val updateAppsBoolList: List<Boolean> = updateAppsBool.toList()
        val updateAppsBoolSet: MutableList<String> = mutableListOf()
        for (i in updateAppsBoolList.indices) {
            updateAppsBoolSet.add(i.toString() + updateAppsBoolList[i].toString())
        }

        with(applicationContext().getSharedPreferences(sharedPrefName, MODE_PRIVATE).edit()) {
            this?.putStringSet("appsUpdate", updateAppsName)
            this?.putStringSet("boolUpdate", updateAppsBoolSet.toSet())
            this?.apply()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) { // Main function
        super.onCreate(savedInstanceState)
        sharedPref = getSharedPreferences(sharedPrefName, MODE_PRIVATE)

        try {
            if (Credentials().get(0) == null) { // test if credentials have already been registered
                loginPage() // if no ask for
            } else {
                mainPage() // if yes go to main page
            }
        } catch (_: Throwable) {
            loginPage()
        }
    }

    private fun loginPage() {
        setContentView(R.layout.activity_login) // show login page
        this.title = getString(R.string.app_name)

        val userInput = findViewById<EditText>(R.id.usernameInput)
        val passwordInput = findViewById<EditText>(R.id.passwordInput)
        val buttonInput = findViewById<Button>(R.id.loginButton)

        fun changeButtonStatus() { // function who enable/disable the login button
            val text1: String = userInput.text.toString()
            val text2: String = passwordInput.text.toString()
            buttonInput.isEnabled = text1.isNotEmpty() && text2.isNotEmpty()
        }

        userInput.addTextChangedListener(object : TextWatcher { // looking user input changes
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(editable: Editable) {
                changeButtonStatus()
            }
        })

        passwordInput.addTextChangedListener(object :
            TextWatcher { // looking password input changes
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(editable: Editable) {
                changeButtonStatus()
            }
        })

        buttonInput.setOnClickListener { // show main page when clicking the login button
            val user = userInput.text.toString()
            val password = passwordInput.text.toString()

            Credentials().store(user, password)
            mainPage()
        }
    }

    private fun mainPage(toDelete: String? = null) {
        if (toDelete == null) {
            setContentView(R.layout.activity_main) // display main page
            if (!isWorkScheduled(applicationContext)) newJob(applicationContext) // run background job if not already running
            if (!settingsButtonVisible) { // check if the settings button isn't already showed
                Handler(Looper.getMainLooper()).postDelayed({
                    toggleSettingsButtonVisibility()
                }, 100) // delay to add button settings
            }
        }
        val user = Credentials().get(0).toString()
        this.title = "${getString(R.string.app_name)} (${getString(R.string.connected_as)} $user)"
        val valuesRecyclerView = getValuesRecyclerView() // list of all the element in the main page
        val checkedItemListOfApps = getValuesAppNeedToBeUpdated()

        class Adapter(private val values: List<String>) :
            RecyclerView.Adapter<Adapter.ViewHolder>() {
            override fun getItemCount() = values.size

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int,
            ): ViewHolder { // add viewHolder to the main page
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_view, parent, false)
                return ViewHolder(itemView)
            }

            override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                holder.button?.text = values[position]
            }

            inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
                var button: Button? = null

                init {
                    button = itemView?.findViewById(R.id.text_list_item)
                    button?.setOnLongClickListener {
                        val builder: AlertDialog.Builder =
                            AlertDialog.Builder(instance)
                        builder.setTitle("${getString(R.string.config)} ${button?.text} ?")
                        builder.setPositiveButton(R.string.remove) { _, _ ->
                            instance?.mainPage(
                                button?.text.toString()
                            )
                            checkedItemListOfApps[button?.text.toString()] = false
                            storeValuesAppNeedToBeUpdated(checkedItemListOfApps)
                        }
                        val appInstalled = isItAnInstalledApp(button?.text.toString())
                        if (appInstalled != null) {
                            val checkedItems = booleanArrayOf(
                                checkedItemListOfApps[button?.text.toString()] == true
                            )
                            builder.setMultiChoiceItems(
                                arrayOf(getString(R.string.updateCheck)),
                                checkedItems
                            ) { _, _, isChecked ->
                                checkedItemListOfApps[button?.text.toString()] = isChecked
                                storeValuesAppNeedToBeUpdated(checkedItemListOfApps)
                            }
                            builder.setNegativeButton(R.string.forceUpdate) { _, _ ->
                                Notif().work(applicationContext,
                                    button?.text.toString())
                            }
                        }
                        builder.setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                        builder.show()
                        true
                    }
                    button?.setOnClickListener {
                        val password = Credentials().get(1)
                        if (password != null) { // call script
                            callScrapper(user, password.toString(), button?.text.toString())
                        } else {
                            Toast.makeText(
                                applicationContext,
                                R.string.notConnected,
                                Toast.LENGTH_LONG
                            )
                                .show()
                            loginPage()
                        }
                    }
                }
            }

        }

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        if (toDelete != null) {
            valuesRecyclerView.remove(toDelete)

            // read apps from the app preference
            sharedPref!!.edit().putStringSet("apps", valuesRecyclerView.toSet()).apply()
            recyclerView.adapter = Adapter(valuesRecyclerView)
        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = Adapter(valuesRecyclerView)


        fun addApp(app: String) {
            valuesRecyclerView.add(app)

            // read apps from the app preference
            sharedPref!!.edit().putStringSet("apps", valuesRecyclerView.toSet()).apply()
            recyclerView.adapter = Adapter(valuesRecyclerView)

        }

        val addButton = findViewById<FloatingActionButton>(R.id.addButton)
        addButton.setOnClickListener {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.newAppDialogTitle)
            val input = EditText(this)
            input.inputType = InputType.TYPE_CLASS_TEXT
            builder.setView(input)
            builder.setPositiveButton(R.string.validate) { _, _ -> addApp(input.text.toString()) }
            builder.setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }

            builder.show()
        }

        // easter egg
        addButton.setOnLongClickListener {
            Toast.makeText(this@MainActivity, R.string.dropBro, Toast.LENGTH_SHORT)
                .show()
            true
        }
    }

    fun isItAnInstalledApp(app: String): String? {
        var appName = app
        val registeredAnswered: Map<String, String> = Gson().fromJson(
            assets.open("appNames.json").bufferedReader()
                .use { it.readText() },
            object : TypeToken<Map<String, String>>() {}.type
        )
        for ((key, value) in registeredAnswered) appName =
            if (Regex(appName.lowercase()).containsMatchIn(key)) value else appName.lowercase()
        for (packageInfo in packageManager.getInstalledPackages(PackageManager.GET_META_DATA)) {
            if (Regex(appName).containsMatchIn(packageInfo.packageName)) return packageInfo.versionName
        }
        return null
    }

    private fun callScrapper(
        user: String,
        password: String,
        app: String,
        captchaID: String? = null,
        captchaResponse: String? = null,
    ) {
        if (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.INTERNET)
            != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(this@MainActivity, R.string.internetPermission, Toast.LENGTH_SHORT)
                .show()
            this.finishAffinity()
        } else {
            runBlocking { // GlobalScope.launch {
                val returns: MutableList<Map<String, String?>>? = Scraper(
                    user,
                    password,
                    app,
                    captchaID = captchaID,
                    captchaResponse = captchaResponse
                ).search()
                if (returns != null) { // if job is needed
                    if (returns[0].containsKey("errorNotConnected")) {
                        Toast.makeText(
                            instance,
                            "${getString(R.string.connectionFailed)}\n${getString(R.string.credentialsDeleted)}.",
                            Toast.LENGTH_LONG
                        ).show()
                        loginPage()
                    }
                    if (returns[0].containsKey("loginAttemptsExceeded")) {
                        val returnsCaptchaQuestion = returns[0]["question"]
                        val returnsCaptchaID = returns[0]["qaConfirmID"]
                        if (returnsCaptchaQuestion == null) callScrapper(user, password, app)
                        val registeredAnswered: Map<String, String> = Gson().fromJson(
                            assets.open("captcha.json").bufferedReader()
                                .use { it.readText() },
                            object : TypeToken<Map<String, String>>() {}.type
                        )
                        for ((key, value) in registeredAnswered) {
                            if (returnsCaptchaQuestion == key) {
                                println("${applicationContext().getString(R.string.knownCaptcha)}...")
                                val now = System.currentTimeMillis()
                                if ((now - timeOfLastToast) >= 2000) {
                                    Toast.makeText(
                                        applicationContext(),
                                        "${applicationContext().getString(R.string.knownCaptcha)}...",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                    timeOfLastToast = now
                                }
                                return@runBlocking callScrapper(
                                    user,
                                    password,
                                    app,
                                    returnsCaptchaID,
                                    value
                                )
                            }
                        }
                        val builder: AlertDialog.Builder = AlertDialog.Builder(
                            instance
                        )
                        builder.setTitle("Captcha")
                        builder.setMessage(returnsCaptchaQuestion)
                        val input = EditText(instance)
                        input.inputType = InputType.TYPE_CLASS_TEXT
                        builder.setView(input)
                        builder.setPositiveButton(R.string.validate) { _, _ ->
                            callScrapper(
                                user,
                                password,
                                app,
                                returnsCaptchaID,
                                input.text.toString()
                            )
                        }
                        builder.setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                        builder.show()
                    }
                    if (returns[0].containsKey("noSID"))
                        Toast.makeText(instance, R.string.noSID, Toast.LENGTH_SHORT).show()
                    if (returns[0].containsKey("badSID"))
                        Toast.makeText(
                            instance,
                            "${getString(R.string.badSID)}...",
                            Toast.LENGTH_SHORT
                        ).show()
                    if (returns[0].containsKey("noResults"))
                        Toast.makeText(
                            instance,
                            "${getString(R.string.noResults)}...",
                            Toast.LENGTH_SHORT
                        ).show()
                    if (returns[0].containsKey("gotResults")) {
                        returns.removeFirst()
                        Toast.makeText(
                            instance,
                            "${returns.size} ${getString(R.string.gotResults)} !",
                            Toast.LENGTH_SHORT
                        ).show()
                        listInfos = returns
                        showAppListPage()
                    }
                }
            }
        }
    }

    private fun showAppListPage() {
        setContentView(R.layout.activity_app_list) // display list of app version page
        if (!settingsButtonVisible) toggleSettingsButtonVisibility() // check if the settings button isn't already showed and show it if necessary
        inAppList = true
        class Adapter(private val values: List<String>) :
            RecyclerView.Adapter<Adapter.ViewHolder>() {
            override fun getItemCount() = values.size

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int,
            ): ViewHolder { // add viewHolder to the parent page
                val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_view_app_list, parent, false)
                return ViewHolder(itemView)
            }

            override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                holder.button?.text = values[position]
            }

            inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
                var button: Button? = null

                init {
                    button = itemView?.findViewById(R.id.text_list_item_app_list)
                    button?.setOnClickListener {
                        inAppList = false
                        appMobilismInfos =
                            listInfos!![Regex("""\d+""").findAll(button?.text.toString())
                                .map { it.groupValues[0] }.toList()[0].toInt() - 1]
                        showAppInfoPage()
                    }
                }
            }
        }

        val recyclerViewAppList: RecyclerView =
            findViewById(R.id.recyclerViewAppList) // get recyclerview
        recyclerViewAppList.layoutManager = LinearLayoutManager(this)
        val listInfosNames = mutableListOf<String>()
        for (i in listInfos!!.indices) {  // get all apps name
            val tmp =
                "${i + 1} - ${listInfos!![i]["title"]} (${listInfos!![i]["author"]})\n${listInfos!![i]["date"]}"
            listInfosNames += tmp
        }
        recyclerViewAppList.adapter = Adapter(listInfosNames)
    }

    @SuppressLint("SetTextI18n")
    private fun showAppInfoPage() {
        val link = appMobilismInfos!!["link"]
        if (link != null) {
            val infoApp: MutableMap<String, String?>
            var links: MutableMap<String, List<String>> = mutableMapOf()
            var errorHappen = false
            runBlocking {
                infoApp = Scraper().parseInfos(link)
                try {
                    links = Scraper().parseDownloadLinks(infoApp["downloadLinks"]!!)
                } catch (e: Exception) {
                    Toast.makeText(
                        instance,
                        "${getString(R.string.noLinkFound)}...",
                        Toast.LENGTH_SHORT
                    ).show()
                    errorHappen = true
                }
            }
            if (errorHappen) return else {
                setContentView(R.layout.activity_app_infos) // display app info detailed page
                if (!settingsButtonVisible) toggleSettingsButtonVisibility() // check if the settings button isn't already showed and show it if necessary
                inAppInfo = true
            }
            class Adapter(private val values: List<String>) :
                RecyclerView.Adapter<Adapter.ViewHolder>() {
                override fun getItemCount() = values.size

                override fun onCreateViewHolder(
                    parent: ViewGroup,
                    viewType: Int,
                ): ViewHolder { // add viewHolder to the parent page
                    val itemView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_view_app_download, parent, false)
                    return ViewHolder(itemView)
                }

                override fun onBindViewHolder(holder: ViewHolder, position: Int) {
                    holder.button?.text = values[position]
                }

                inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
                    var button: Button? = null

                    init {
                        button = itemView?.findViewById(R.id.download_button)
                        button?.setOnClickListener {
                            val splitText = button?.text.toString().split(Regex(" - "))
                            startActivity(
                                Intent(
                                    Intent.ACTION_VIEW, Uri.parse(
                                        links[splitText[0]]!![splitText[1].replace("\\s".toRegex(),
                                            "").toInt() - 1]
                                    )
                                )
                            )
                        }
                    }
                }
            }

            val title = findViewById<TextView>(R.id.textViewAppName)
            title.text = appMobilismInfos!!["title"]
            title.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://forum.mobilism.org$link")))
            }
            findViewById<TextView>(R.id.textViewAppAuthor).text = appMobilismInfos!!["author"]
            findViewById<TextView>(R.id.textViewAppDate).text = appMobilismInfos!!["date"]
            val changelogs = findViewById<TextView>(R.id.textViewAppChangelogs)
            changelogs.text = infoApp["changelogs"]
            changelogs.movementMethod = ScrollingMovementMethod()

            val listDownloads = mutableListOf<String>()
            val recyclerViewAppDownloads: RecyclerView =
                findViewById(R.id.recyclerViewAppDownloads) // get recyclerview
            recyclerViewAppDownloads.layoutManager = LinearLayoutManager(this)
            val displayMetrics = DisplayMetrics()
            recyclerViewAppDownloads.layoutParams.height = displayMetrics.heightPixels / 3
            for (arch in links.keys) {
                for ((count, _) in links[arch]!!.withIndex()) {
                    listDownloads += "$arch - ${count + 1}"
                }
                recyclerViewAppDownloads.adapter = Adapter(listDownloads)
            }
        } else Toast.makeText(instance, "${getString(R.string.noURL)}...", Toast.LENGTH_SHORT)
            .show()
    }

    @SuppressLint("SetTextI18n")
    private fun settingsPage() {
        setContentView(R.layout.activity_settings)
        toggleSettingsButtonVisibility()
        inSettings = true
        val pInfo: PackageInfo =
            applicationContext().packageManager.getPackageInfo(applicationContext().packageName, 0)
        val stringVersion = findViewById<TextView>(R.id.version)
        stringVersion.text = "v${pInfo.versionName}"

        val credentialsButton = findViewById<Button>(R.id.changeCredentialsButton)
        credentialsButton.setOnClickListener { // show main page when clicking the login button
            Credentials().delete()
            Toast.makeText(this@MainActivity, R.string.deletedCredentials, Toast.LENGTH_LONG)
                .show()
            loginPage()
        }

        val sourceCodeButton = findViewById<Button>(R.id.sourcecodeButton)
        sourceCodeButton.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW, Uri.parse(
                        "https://gitlab.com/Mylloon/mobilismScrap/-/tree/android"
                    )
                )
            )
        }
    }

    override fun onResume() {
        super.onResume()
        if (sharedPref!!.getBoolean("firstrun", true)) {
            Credentials().generateKey() // Generate RSA keys
            sharedPref!!.edit().putBoolean("firstrun", false)
                .apply() // first run done, now the next ones won't be "first".
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean { // add settings button
        menuInflater.inflate(R.menu.menu_main, menu)
        settingsButton = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // go to settings page when clicking on settings button
        when (item.itemId) {
            R.id.settingsButton -> {
                settingsPage()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() { // allow user to use back button to go back to main page
        when {
            inSettings -> {
                inSettings = false
                return when {
                    inAppList -> showAppListPage()
                    inAppInfo -> showAppInfoPage()
                    else -> mainPage()
                }
            }
            inAppList -> {
                inAppList = false
                return mainPage()
            }
            inAppInfo -> {
                inAppInfo = false
                return showAppListPage()
            }
            else -> super.onBackPressed()
        }
    }
}
