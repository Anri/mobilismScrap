# MobiDL
## Disclaimer
This branch corresponds to the scraper written in Kotlin *for Android*, check [this branch](https://git.kennel.ml/Anri/mobilismScrap/src/branch/python) for the one written in Python.<br>

## Download
You can download the app in the [release page](https://git.kennel.ml/Anri/mobilismScrap/releases).

Application compatible with Android 12.

### Requirements
Minimum: Android 6.0 (Marshmallow) <br>
Recommended: Android 8.0 (Oreo)
